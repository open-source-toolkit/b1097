# 基于Python+Django的人员管理系统源码

## 项目描述

本项目是一个基于Python和Django框架开发的人员管理系统源码。该项目适合正在学习Django框架的开发者使用，是我作为毕业设计开发的一个完整的人员管理系统。

## 使用说明

### 环境准备

在运行本项目之前，请确保您已经安装了以下软件：

1. **Python**：本项目使用Python 3.x版本。
2. **Django**：Django是一个强大的Web框架，本项目使用Django 3.x版本。
3. **MySQL**：本项目使用MySQL作为数据库。

### 安装步骤

1. **安装Python、Django和MySQL**：
   - 您可以在网上找到详细的安装步骤，或者关注我的后续教程，我将提供详细的安装和配置指南。

2. **创建Django项目和App**：
   - 创建一个名为`Personnel`的Django项目。
   - 在项目中创建一个名为`system`的App。

3. **配置MySQL数据库**：
   - 在MySQL中创建一个名为`Personnel`的数据库。
   - 修改`settings.py`文件中的数据库配置，确保数据库的用户名、密码等信息正确。

4. **执行数据库迁移**：
   - 在终端中执行以下命令以生成数据库迁移文件并应用迁移：
     ```bash
     python manage.py makemigrations
     python manage.py migrate
     ```

5. **创建后台管理用户**：
   - 执行以下命令创建一个超级用户，用于登录Django后台管理系统：
     ```bash
     python manage.py createsuperuser
     ```

6. **启动程序**：
   - 执行以下命令启动Django开发服务器：
     ```bash
     python manage.py runserver
     ```
   - 打开浏览器，访问`http://127.0.0.1:8000/`即可查看程序页面。

## 注意事项

- 本项目是一个学习性质的项目，适合初学者参考和学习Django框架的使用。
- 如果您在安装或配置过程中遇到问题，可以参考网上的教程或关注我的后续文章，我将提供详细的安装和配置指南。

## 联系我

如果您有任何问题或建议，欢迎通过GitHub Issues或邮件与我联系。

---

**感谢您的关注和支持！**